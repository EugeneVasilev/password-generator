import random


def password_generator(length):
    small_letters = 'abcdefghijklmnopqrstyvwxyzABCDEFGHIJKLMNOPQRSTYVWXYZ1234567890~!@#$%^&*(){}-=:"<>'
    return ''.join(random.choices(small_letters, k=length))


while True:
    print('Enter the length of password:')
    ln = input()
    if ln.isdigit():
        print(password_generator(int(ln)))
